#!/bin/bash

set -e

URL=http://localhost:4225
FILE=`mktemp`
EXIT_ON_FAILURE=${EXIT_ON_FAILURE:-yes}

API_KEY=${API_KEY:-1234}

count=0
failure=0

function run() {
    echo Endpoint: $1 $2
    http_response=$(curl -H "access_token: ${API_KEY}" -H 'accept: application/json' -H 'Content-Type: application/json' -s -o $FILE -w "%{http_code}" -X $1 $2 -d "$3")

    # cat $FILE

    if [ $http_response -ne 200 ]; then
        echo Failed
        cat $FILE
        failure=$((failure + 1))
        if [ x"${EXIT_ON_FAILURE}" == "xyes" ];
        then
            exit 1
        fi
    else
        echo Success
    fi
    count=$((count + 1))
    echo
    echo
}

run GET ${URL}/health ''

run GET ${URL}/health-k8s ''

run GET ${URL}/nodes/tester ''

run PUT ${URL}/nodes "{\"name\": \"tester\", \"state\": 1, \"reason\": \"test\"}"

run GET ${URL}/nodes ''

run PUT ${URL}/reservation "{\"name\": \"\", \"nodes\": [\"tester\"], \"start\": null, \"duration_min\": 120, \"users\": [\"root\"], \"flags\": 3}"

res_name=`cat $FILE | tr -d '"'`
echo $res_name

run GET ${URL}/reservations ''

run DELETE ${URL}/reservation/${res_name} ''

run POST ${URL}/reboot "{\"nodes\": [\"tester\"], \"reboot_type\": 0, \"next_state\": 0, \"reason\": \"test\"}"

run GET ${URL}/queue/statistics

run GET ${URL}/job/1234

run DELETE ${URL}/job/1234

run PUT ${URL}/submit "{\"user\": \"calculus\", \"script\": \"#!/bin/bash\\necho Test\\n\"}"

run GET ${URL}/bad-users "{\"for_last_hours\": 24, \"number_users\": 10, \"cpu_used_ratio\": 0.3, \"ram_used_ratio\": 0.3, \"cpu_to_gpu_ratio\": 1.5, \"ram_to_gpu_ratio\": 1.5}"


echo "Test: ${count}"
if [ $failure -eq 0 ]; then
    echo -e "\e[32mFailure: ${failure}\e[0m"
else
    echo -e "\e[31mFailure: ${failure}\e[0m"
fi
