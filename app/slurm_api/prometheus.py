from fastapi import HTTPException
from prometheus_client import Counter, Histogram, start_http_server
from prometheus_client.context_managers import ExceptionCounter

from .config import config
from .logger import logger


# Replace some functions from prometheus-client in order to blacklist
# the expected errors and keep only the unknown ones
class InternalBlacklistExceptionCounter(ExceptionCounter):
    def __exit__(self, typ, value, traceback):
        if value is not None and not isinstance(value, self._exception):
            logger.debug(f"Value: {value}")
            self._counter.inc()
        return False


class BlacklistExceptionCounter(Counter):
    def count_exceptions(self, blacklist_exception):
        self._raise_if_not_observable()
        return InternalBlacklistExceptionCounter(self, blacklist_exception)


class Prometheus:
    """
    WARNING: does not handle multiprocess service
    It seems that the decorators are not properly working with pydantic
    """

    REQUEST_TIME = Histogram(
        "slurm_api_request_processing_seconds", "Time spent processing request"
    )
    SLURM_HEALTH_REQUESTS = Counter(
        "slurm_api_health_requests_to_slurm", "Number of slurm health requests"
    )
    SLURM_REQUESTS = Counter("slurm_api_requests_to_slurm", "Number of slurm requests")

    ERROR = BlacklistExceptionCounter(
        "slurm_api_errors", "Number of errors in slurm-api"
    )

    UNAUTHORIZED_ACCESS = Counter(
        "slurm_api_unauthorized_access", "Number of unauthorized access in slurm-api"
    )

    def __init__(self):
        self.stack = None

    @classmethod
    def time(cls):
        return Prometheus.REQUEST_TIME.time()

    @classmethod
    def increase_number_slurm_requests(cls, value=1):
        Prometheus.SLURM_REQUESTS.inc(value)

    @classmethod
    def increase_number_slurm_health_requests(cls, value=1):
        Prometheus.SLURM_HEALTH_REQUESTS.inc(value)

    @classmethod
    def increase_number_unauthorized_access(cls, value=1):
        Prometheus.UNAUTHORIZED_ACCESS.inc(value)

    @classmethod
    def start(cls):
        start_http_server(config.PROMETHEUS_PORT)

    def __enter__(self):
        self.stack = []
        self.stack.append(Prometheus.ERROR.count_exceptions(HTTPException))
        self.stack.append(Prometheus.REQUEST_TIME.time().__enter__())

    def __exit__(self, exc_type, exc_value, traceback):
        for context in self.stack:
            context.__exit__(exc_type, exc_value, traceback)
        self.stack = None
