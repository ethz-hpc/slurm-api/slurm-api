from fastapi import HTTPException


class NodeNotFound(HTTPException):
    def __init__(self, node):
        detail = f"Node '{node}' not found"
        super().__init__(status_code=404, detail=detail)


class PartitionNotFound(HTTPException):
    def __init__(self, partition):
        detail = f"Partition '{partition}' not found"
        super().__init__(status_code=404, detail=detail)


class SetReservationNodeError(HTTPException):
    def __init__(self, detail):
        super().__init__(status_code=400, detail=detail)


class FailedToProcessRequest(HTTPException):
    def __init__(self, stdout, stderr):
        self.stdout = stdout
        self.stderr = stderr
        super().__init__(status_code=512, detail="Failed to process request")


class JobNotInQueue(HTTPException):
    def __init__(self, job_id):
        super().__init__(
            status_code=400, detail=f"The job {job_id} is not in the queue"
        )


class InvalidAccount(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=400,
            detail="The Slurm account (share) used is invalid or too"
            " much resources requested",
        )


class NodeConfigurationUnavailable(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=400,
            detail="The node configuration is not available on the cluster",
        )


class FailedToGetUserInformations(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=500,
            detail="Failed to get user information from LDAP",
        )


class UserGroupDoesNotExist(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=500,
            detail="Your group does not exist in LDAP. "
            "Please contact your local IT support to fix your account.",
        )


class InvalidUser(HTTPException):
    def __init__(self, user):
        super().__init__(status_code=432, detail=f"The user {user} is invalid")
