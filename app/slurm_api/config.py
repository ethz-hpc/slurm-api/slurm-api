import os

import yaml

from .endpoint import Endpoint
from .logger import logger


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Config:
    __metaclass__ = Singleton

    RUN_DEV = "RUN_DEV" in os.environ

    def __init__(self):
        config_path = os.environ["CONFIG_PATH"]
        with open(config_path, "r") as f:
            data = yaml.safe_load(f)

        # Queries
        Config.BUFFER_SIZE = int(data["bufferSizeMb"]) * int(1e7)

        # Prometheus
        Config.PROMETHEUS_PORT = int(data["prometheus"]["port"])

        # Auth
        if not Config.RUN_DEV:
            Config.AUTH_HEADER_NAME = data["auth"]["headerName"]
            Config.AUTH_PARAMS = data["auth"]["rules"]

            for auth in Config.AUTH_PARAMS:
                if "name" not in auth or "key" not in auth or "endpoints" not in auth:
                    raise Exception(
                        "Error in the configuration file."
                        "For each authentication object, you need 'name', "
                        "'key' and 'endpoints'."
                    )
                for d in auth["endpoints"]:
                    if "method" not in d or "path" not in d:
                        logger.error(f"Endpoint {d}")
                        raise Exception("Each endpoints need 'method' and 'path'")

            self.generate_auth_config()

    def generate_auth_config(self):
        # Create endpoints from config
        output = {}
        if not self.RUN_DEV:
            for auth in self.AUTH_PARAMS:
                name = auth["name"]
                endpoints = []
                for d in auth["endpoints"]:
                    endpoints.append(Endpoint(d["method"], d["path"]))
                output[name] = {"key": auth["key"], "endpoints": endpoints}
        self.AUTH_RULES = output


config = Config()
