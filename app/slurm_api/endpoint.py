import re

from starlette.datastructures import URL


class Endpoint:
    def __init__(self, method, path):
        if isinstance(path, URL):
            path = path._url
        self.method = method
        self.path = path
        self.regex = path.replace("*", r"[^\/]+")

    def __eq__(self, other):
        """This is not symmetric for security reason"""
        if self.method != other.method:
            return False

        if self.path == other.path:
            return True

        m1 = re.search(f"^{self.regex}$", other.path)
        return m1 is not None
