from fastapi import FastAPI

from .authentication import authentication
from .prometheus import Prometheus
from .routers.health import router as health
from .routers.slurm import router as slurm

LOG_LEVEL = "LOG_LEVEL"

app = FastAPI()

# Middleware
app.middleware("http")(authentication)

# Routers
app.include_router(health)
app.include_router(slurm)


# Prometheus
Prometheus.start()
