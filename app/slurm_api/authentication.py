from fastapi import Request, status
from fastapi.responses import JSONResponse

from .config import config
from .endpoint import Endpoint
from .logger import logger
from .prometheus import Prometheus


def unauthorized_access(request: Request):
    headers = request.headers.mutablecopy()
    if config.AUTH_HEADER_NAME in headers:
        logger.warning("Hidding api key")
        del headers[config.AUTH_HEADER_NAME]
    logger.error(
        f"Unauthorized request: url={request.url}\n\theaders={headers}"
        f"\n\tclient={request.client}"
    )
    Prometheus.increase_number_unauthorized_access()
    return JSONResponse(
        content={"message": "not authorized"}, status_code=status.HTTP_401_UNAUTHORIZED
    )


async def authentication(request: Request, call_next):
    if config.RUN_DEV or request.url.path in ["/health", "/health-k8s"]:
        return await call_next(request)

    # Check the key
    headers = request.headers
    if config.AUTH_HEADER_NAME not in headers:
        return unauthorized_access(request)

    key = headers[config.AUTH_HEADER_NAME]
    username = None
    for name, auth in config.AUTH_RULES.items():
        if key == auth["key"]:
            username = name
            break

    if username is None:
        return unauthorized_access(request)

    # Check the endpoint
    allowed_endpoints = config.AUTH_RULES[username]["endpoints"]
    endpoint = Endpoint(request.method, request.url.path)
    if endpoint in allowed_endpoints:
        return await call_next(request)
    else:
        return unauthorized_access(request)
