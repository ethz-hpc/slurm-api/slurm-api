from fastapi import APIRouter, HTTPException

from slurm_api.slurm import Slurm

router = APIRouter()
slurm = Slurm()


@router.get("/health")
async def health() -> bool:
    return True


@router.get("/health-k8s")
async def health_k8s() -> bool:
    ok = await slurm.check_health()
    if not ok:
        raise HTTPException(status_code=500, detail="Slurm is not working")

    return True
