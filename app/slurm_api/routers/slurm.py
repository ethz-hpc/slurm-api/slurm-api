from typing import List, Union

from fastapi import APIRouter, Path, Query

from slurm_api.logger import logger
from slurm_api.prometheus import Prometheus
from slurm_api.slurm import Slurm
from slurm_api_schema.job import JobLongReportV2, JobReportV2, JobResponseV2, JobState
from slurm_api_schema.node import NODE_REGEX, Node, NodeResponse
from slurm_api_schema.partition import PARTITION_REGEX, Partition
from slurm_api_schema.reboot import Reboot
from slurm_api_schema.reservation import (
    RESERVATION_REGEX,
    Reservation,
    ReservationResponse,
)
from slurm_api_schema.statistics import QueueStatistics
from slurm_api_schema.submit import Submit
from slurm_api_schema.user import USER_REGEX, User

router = APIRouter()


@router.get("/nodes/{node_name}")
async def get_nodes(node_name: str = Path(regex=NODE_REGEX)) -> NodeResponse:
    with Prometheus():
        logger.info(f"Requesting /nodes with '{node_name}'")

        list_nodes = await Slurm.get_nodes(node_name)
        return list_nodes[0]


@router.get("/nodes")
async def get_all_nodes() -> List[NodeResponse]:
    with Prometheus():
        list_nodes = await Slurm.get_nodes(None)
        return list_nodes


@router.get("/partitions/{partition_name}")
async def get_partitions(
    partition_name: str = Path(regex=PARTITION_REGEX),
) -> Partition:
    with Prometheus():
        return await Slurm.get_partitions(partition_name)[0]


@router.get("/partitions")
async def get_all_partitions() -> List[Partition]:
    with Prometheus():
        return await Slurm.get_partitions(None)


@router.put("/nodes")
async def set_node(node: Node) -> bool:
    with Prometheus():
        logger.info(f"Setting node {node.name} with '{node}'")

        await Slurm.set_node(node)
        return True


@router.get("/reservations")
async def get_reservations() -> List[ReservationResponse]:
    with Prometheus():
        return await Slurm.get_reservations()


@router.put("/reservation")
async def put_reservation(reservation: Reservation) -> str:
    with Prometheus():
        return await Slurm.set_reservation(reservation)


@router.delete("/reservation/{reservation_name}")
async def delete_reservation(
    reservation_name: str = Path(regex=RESERVATION_REGEX),
) -> bool:
    with Prometheus():
        await Slurm.delete_reservation(reservation_name)
        return True


@router.post("/reboot")
async def reboot(reboot: Reboot) -> bool:
    with Prometheus():
        logger.info(f"Rebooting nodes '{reboot.nodes}'")

        await Slurm.reboot(reboot)
        return True


@router.get("/queue/statistics")
async def queue_statistics() -> QueueStatistics:
    with Prometheus():
        return await Slurm.get_queue_statistics()


@router.put("/submit")
async def submit(submit: Submit) -> int:
    with Prometheus():
        return await Slurm.submit_job(submit)


@router.delete("/job/{job_id}")
async def cancel_job(job_id: int):
    with Prometheus():
        return await Slurm.cancel_job(job_id)


@router.get("/v2/job/{job_id}")
async def get_job_v2(job_id: int) -> JobResponseV2:
    with Prometheus():
        return await Slurm.job_status(job_id)


@router.get("/v2/job/long-report/{job_id}")
@router.get("/v2/job/long-report/{job_id}/{job_subtask}")
async def get_long_job_report_v2(
    job_id: int, job_subtask: Union[int, None] = None
) -> JobLongReportV2:
    with Prometheus():
        return await Slurm.job_long_report(job_id, job_subtask)


@router.get("/v2/jobs/report")
async def get_jobs_report_v2(
    for_last_hours: int = Query(
        24, description="Time interval over which to check the jobs"
    ),
    user: Union[str, None] = Query(None, regex=USER_REGEX),
    state: Union[int, None] = None,
) -> list[JobReportV2]:
    with Prometheus():
        if state is not None:
            state = JobState(state)
        return await Slurm.get_jobs_report(for_last_hours, user=user, state=state)


@router.get("/users")
async def get_users() -> list[User]:
    with Prometheus():
        return await Slurm.get_users()
