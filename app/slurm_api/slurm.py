import grp
import logging
import pwd
from asyncio import sleep
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile
from typing import List, Union

from async_lru import alru_cache
from fastapi import HTTPException, status

from slurm_api_schema.job import (
    JobLongReportV2,
    JobReason,
    JobReportV2,
    JobResponseV2,
    JobState,
)
from slurm_api_schema.node import Node, NodeResponse, NodeState
from slurm_api_schema.partition import Partition, PartitionState
from slurm_api_schema.reboot import Reboot, RebootNextState, RebootType
from slurm_api_schema.reservation import (
    Reservation,
    ReservationFlag,
    ReservationResponse,
)
from slurm_api_schema.statistics import QueueStatistics
from slurm_api_schema.submit import Submit
from slurm_api_schema.user import User

from .config import config
from .exceptions import (
    FailedToGetUserInformations,
    FailedToProcessRequest,
    InvalidAccount,
    InvalidUser,
    JobNotInQueue,
    NodeConfigurationUnavailable,
    NodeNotFound,
    PartitionNotFound,
    SetReservationNodeError,
    UserGroupDoesNotExist,
)
from .logger import logger
from .utils import (
    convert_slurm_list_to_dict,
    convert_time_to_timedelta,
    extract_compact_node_names,
    extract_job_report,
    extract_regex,
    get_memory_size,
    submit_command,
)


class Slurm:
    @staticmethod
    async def check_health() -> bool:
        command = ["scontrol", "ping"]
        stdout, stderr = await submit_command(
            command, is_health=True, dev_output="tests/data/scontrol_ping.txt"
        )

        ok = "UP" in stdout
        if not ok:
            logger.warning(f"Failed to reach slurm: {stdout}\n{stderr}")

        # Ensure that we have some nodes up
        return ok

    @staticmethod
    async def submit_job(submit: Submit) -> int:
        if submit.user == "root":
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

        with NamedTemporaryFile("w") as f:
            f.write(submit.script)
            f.flush()
            # Need --export=NONE to avoid giving away passwords in env
            if config.RUN_DEV:
                gid = 1000
            else:
                try:
                    gid = pwd.getpwnam(submit.user).pw_gid
                except Exception:
                    raise FailedToGetUserInformations()
                # Ensure that the group exist
                try:
                    grp.getgrgid(gid)
                except KeyError:
                    raise UserGroupDoesNotExist()

            command = [
                "sudo",
                "sbatch",
                "--export=NONE",
                f"--uid={submit.user}",
                f"--gid={gid}",
                f.name,
            ]

            try:
                stdout, _ = await submit_command(
                    command, is_slurm=True, dev_output="tests/data/sbatch.txt"
                )
            except FailedToProcessRequest as ex:
                if "Invalid account" in ex.stderr:
                    raise InvalidAccount()
                elif "Requested node configuration" in ex.stderr:
                    raise NodeConfigurationUnavailable()
                else:
                    raise ex

        return int(stdout.split(" ")[-1])

    @staticmethod
    async def cancel_job(job_id: int):
        command = ["scancel", str(job_id)]
        await submit_command(command, is_slurm=True)

    @staticmethod
    async def job_status(job_id: int) -> JobResponseV2:
        form = [
            "Name:100",
            "State:20",
            "ReasonList:500",
            "Threads:20",
            "NumCPUs:20",
            "NumNodes:20",
            "MinMemory:20",
            "StartTime:20",
            "TimeLeft:20",
            "TimeLimit:20",
            "SubmitTime:20",
        ]

        DELIMITER = "SPLIT"

        form_txt = f" {DELIMITER} ,".join(form)
        command = ["squeue", "-j", str(job_id), "-h", f"--Format={form_txt}"]
        try:
            stdout, _ = await submit_command(
                command, is_slurm=True, dev_output="tests/data/squeue_job.txt"
            )
        except FailedToProcessRequest as ex:
            if "Invalid job id specified" in ex.stderr:
                raise JobNotInQueue(job_id)
            else:
                raise ex

        # Extract only correct id
        if config.RUN_DEV:
            found = False
            for line in stdout.split("\n"):
                job = int(line.split(DELIMITER)[-1])
                if job == job_id:
                    stdout = line
                    found = True
                    break
            if not found:
                raise Exception("Job id not found")

        # Ensure that the output is not empty
        if len(stdout.rstrip()) == 0:
            raise JobNotInQueue(job_id)

        # Extract the data
        # Remove multiple white spaces
        data = stdout.split(DELIMITER)
        data = [d.rstrip().lstrip() for d in data]

        # Ensure that we don't interpret the data badly
        # +1 for jobid
        while len(data) != (len(form) + config.RUN_DEV):
            # Reason with multiple words
            if "(" in stdout and ")" in stdout:
                # Remove the reason
                tmp = stdout.split("(")[0].rstrip()
                tmp += stdout.split(")")[-1]

                # Generate data
                data = tmp.split(" ")
                reason_raw = stdout.split("(")[-1].split(")")[0]
                data.insert(2, f"({reason_raw})")

                logger.debug(f"Data: {data}")
                break

            raise Exception(f"{data}, {len(data)}, {len(form)}")

        # Get the values
        name = data[0]
        state = data[1]
        reason = data[2]
        reason_raw = data[2]
        threads = data[3]
        cpus = data[4]
        nodes = data[5]
        mem = data[6]
        start = data[7]
        time_left = data[8]
        time_limit = data[9]
        submit_time = datetime.fromisoformat(data[10])

        # Convert the fields
        state = JobState.get_state_from_text(state)
        reason = JobReason.get_reason_from_text(reason)
        threads = -1 if threads == "*" else int(threads)
        cpus = int(cpus)
        nodes = int(nodes)
        if mem[-1] not in ["M", "G"]:
            raise Exception("Unknown unit")
        mem = (1000 if mem[-1] == "G" else 1) * float(mem[:-1])
        start = None if start == "N/A" else datetime.fromisoformat(start)
        time_left = (
            convert_time_to_timedelta(time_left) if time_left != "INVALID" else None
        )
        time_limit = convert_time_to_timedelta(time_limit)

        # Prepare output
        return JobResponseV2(
            name=name,
            job_id=job_id,
            state=state,
            reason=reason,
            reason_raw=reason_raw,
            threads=threads,
            cpus=cpus,
            nodes=nodes,
            memory_mb=int(mem),
            start=start,
            time_left=time_left,
            time_limit=time_limit,
            submit_time=submit_time,
        )

    @staticmethod
    async def reboot(reboot: Reboot):
        command = ["scontrol", "reboot"]
        if reboot.reboot_type == RebootType.ASAP:
            command.append("ASAP")

        if reboot.next_state != RebootNextState.NONE and reboot.next_state is not None:
            command.append(f"nextstate={reboot.next_state.name}")

        if reboot.reason is not None:
            command.append(f"reason={reboot.reason}")

        for node in reboot.nodes:
            command.append(node)

        await submit_command(command, is_slurm=True)

    @staticmethod
    async def set_reservation(reservation: Reservation) -> str:
        if len(reservation.nodes) == 0:
            raise SetReservationNodeError("Cannot request a reservation with 0 node")

        command = ["scontrol", "create", "reservation"]
        flags = ReservationFlag.NONE

        # Name
        if reservation.name:
            command.append(f"ReservationName={reservation.name}")

        # Start time
        if reservation.start:
            # Avoid printing microseconds
            start = reservation.start.isoformat(timespec="seconds")
        else:
            start = "now"
            flags = flags | ReservationFlag.TIME_FLOAT
        command.append(f"starttime={start}")

        # Duration
        if reservation.duration_min < 0:
            duration = "infinite"
        else:
            duration = str(reservation.duration_min)
        command.append(f"duration={duration}")

        # Users
        if len(reservation.users) == 0:
            users = "root"
        else:
            users = ",".join(reservation.users)
        command.append(f"user={users}")

        # Nodes
        nodes = ",".join(reservation.nodes)
        command.append(f"nodes={nodes}")

        # Flags
        flags = flags | reservation.flags
        if flags != ReservationFlag.NONE:
            flags = ReservationFlag.get_text(flags)
            command.append(f"flags={flags}")

        stdout, _ = await submit_command(
            command, is_slurm=True, dev_output="tests/data/scontrol_set_reservation.txt"
        )

        # Prepare output
        name = stdout.split(" ")[-1].rstrip()
        return name

    @staticmethod
    async def get_reservations() -> List[ReservationResponse]:
        command = ["scontrol", "show", "reservation"]
        stdout, _ = await submit_command(
            command, is_slurm=True, dev_output="tests/data/scontrol_reservation.txt"
        )

        reservations = []
        for line in stdout.split("\n\n"):
            if len(line.strip().rstrip()) == 0 or "No reservations" in line:
                continue

            # Extract all info
            name = extract_regex(r"(?<=ReservationName\=)[^ ]+", line)
            nodes = extract_regex(r"(?<=Nodes\=)[^ ]+", line)
            start = extract_regex(r"(?<=StartTime\=)[^ ]+", line)
            end = extract_regex(r"(?<=EndTime\=)[^ ]+", line)
            users = extract_regex(r"(?<=Users\=)[^ ]+", line)
            flags = extract_regex(r"(?<=Flags\=)[^ ]+", line)

            if start is None or end is None:
                raise Exception("Cannot find a start / end for the reservation")

            if users is None:
                raise Exception("Cannot find a user for the reservation")

            if name is None:
                raise Exception("Cannot find a name for the reservation")

            # Convert variables
            if end < start:
                duration = -1
            else:
                start = datetime.fromisoformat(start)
                end = datetime.fromisoformat(end)
                duration = int((end - start).total_seconds() / 60)

            nodes = extract_compact_node_names(nodes)
            users = users.split(",")
            flags = ReservationFlag.get_flags(flags)

            res = ReservationResponse(
                name=name,
                nodes=nodes,
                start=start,
                duration_min=duration,
                users=users,
                flags=flags,
            )

            reservations.append(res)
        return reservations

    @staticmethod
    async def delete_reservation(name: str) -> None:
        command = ["scontrol", "delete", f"ReservationName={name}"]

        # There is no output from this command
        await submit_command(command, is_slurm=True)

    @staticmethod
    @alru_cache(ttl=60)
    async def get_nodes(node_name: str | None = None) -> List[NodeResponse]:
        """
        This function needs to be static for the cache to work
        """
        command = ["scontrol", "show", "node"]
        if node_name is not None:
            command.append(node_name)

        stdout, _ = await submit_command(
            command, is_slurm=True, dev_output="tests/data/scontrol_node.txt"
        )

        if node_name is not None and "not found" in stdout:
            raise NodeNotFound(node_name)

        output = []
        for index, line in enumerate(stdout.split("\n\n")):
            # Give back the control in order to deal with other requests
            if index % 100 == 0:
                await sleep(0)

            if len(line) == 0:
                continue

            # Extract name
            name = extract_regex(r"(?<=NodeName\=)[^ ]+", line)

            # With dev, we cannot request a single node, so process everything
            if config.RUN_DEV and node_name is not None and name != node_name:
                continue

            # Extract values
            features = extract_regex(r"(?<=AvailableFeatures\=)[^ ]+", line)
            resources = extract_regex(r"(?<=Gres\=)[^ ]+", line)
            cpus = extract_regex(r"(?<=CPUTot\=)[^ ]+", line)
            reason = extract_regex(r"(?<=Reason\=).*", line)

            exact_ram = extract_regex(r"(?<=RealMemory\=)[^ ]+", line)
            if exact_ram is None:
                raise Exception("Cannot find the ram of the node")

            rounding = 16000
            ram = round(int(exact_ram) / rounding) * rounding

            boot_time = extract_regex(r"(?<=BootTime\=)[^ ]+", line)
            if boot_time is None:
                raise Exception("Cannot find boot time of the node")
            if boot_time == "None":
                boot_time = None
            else:
                boot_time = datetime.fromisoformat(boot_time)

            state = extract_regex(r"(?<=State\=)[^ ]+", line)
            if state is None:
                raise Exception("Cannot find the state of the node")
            state = NodeState.get_state_from_text(state)

            tres = extract_regex(r"(?<=CfgTRES\=)[^ ]+", line)
            if tres is None:
                raise Exception("Cannot find the TRES of the node")
            tres = tres.split(",")
            tres = {t.split("=")[0]: t.split("=")[1] for t in tres}
            res_name = "gres/gpu"
            number_gpus = 0 if res_name not in tres else int(tres[res_name])
            gpu_mem_name = "gres/gpumem"
            gpu_memory_per_gpu_mb = (
                "0" if gpu_mem_name not in tres else tres[gpu_mem_name]
            )
            gpu_memory_per_gpu_mb = get_memory_size(gpu_memory_per_gpu_mb) / 1024**2

            # Create the node
            group = f"{features},{resources},cpus={cpus},ram={ram}"
            kwargs = {
                "name": name,
                "state": state,
                "group": group,
                "boot_time": boot_time,
                "memory_mb": exact_ram,
                "gpu_memory_per_gpu_mb": gpu_memory_per_gpu_mb,
                "number_cores": cpus,
                "number_gpus": number_gpus,
            }
            if reason is not None:
                kwargs["reason"] = reason

            node = NodeResponse(**kwargs)

            output.append(node)

        # With dev, we cannot request a single node, so process everything
        if config.RUN_DEV and node_name is not None and len(output) == 0:
            raise NodeNotFound(node_name)

        return output

    @staticmethod
    async def set_node(node: Node):
        if node.state is None:
            raise Exception("Cannot set a node with a none state")

        command = [
            "scontrol",
            "update",
            f"NodeName={node.name}",
            f"State={node.state.name}",
        ]
        if node.reason:
            command.append(f"Reason={node.reason}")

        await submit_command(command, is_slurm=True)

    @staticmethod
    async def get_queue_statistics() -> QueueStatistics:
        command = ["squeue", "-h", "--states=PENDING", "--format='%u %R'"]
        stdout, _ = await submit_command(
            command, is_slurm=True, dev_output="tests/data/squeue_pending.txt"
        )

        by_users = {}
        by_reasons = {}
        number_suspicious_priority = 0
        for line in stdout.split("\n"):
            try:
                user, reason = line.split(" ")
            except ValueError:
                logging.debug(f"Failed to process {line}")
                continue
            reason = JobReason.get_reason_from_text(reason)
            if reason == JobReason.Priority and user in by_users:
                number_suspicious_priority += 1
            by_users[user] = by_users.get(user, 0) + 1
            by_reasons[reason] = by_reasons.get(reason, 0) + 1

        return QueueStatistics(
            users=by_users,
            reasons=by_reasons,
            suspicious_priority=number_suspicious_priority,
        )

    @staticmethod
    async def get_jobs_report(
        for_last_hours: int,
        user: Union[str, None],
        state: Union[JobState, None],
    ) -> List[JobReportV2]:
        fields = [
            "User",
            "JobName",
            "NNodes",
            "NodeList",
            "AllocCPUs",
            "TotalCPU",
            "ElapsedRaw",
            "ReqMem",
            "MaxRSS",
            "AllocTRES",
            "JobID",
            "TRESUsageInTot",
            "State",
            "Reason",
            "Start",
            "Timelimit",
            "ConsumedEnergyRaw",
            "Submit",
            "NTask",
        ]
        cmd = [
            "sacct",
            "-P",
            "--format",
            ",".join(fields),
            "-S",
            f"now-{for_last_hours}hours",
            "-E",
            "now",
            "-n",
            "--delimiter= SPLIT ",
        ]

        # User
        if user is not None:
            cmd.append(f"--user={user}")
        else:
            cmd.append("--allusers")

        # State
        if state is not None:
            state_str = str(state).split(".")[-1]
            state_str = state_str.replace("|", ",")
            cmd.append(f"--state={state_str}")

        try:
            stdout, _ = await submit_command(
                cmd, is_slurm=True, dev_output="tests/data/sacct.txt"
            )
        except FailedToProcessRequest as ex:
            if "Invalid user id" in ex.stderr:
                raise InvalidUser(user)
            else:
                raise ex

        # Get info from nodes
        nodes_info = await Slurm.get_nodes()
        nodes_info = {n.name: n for n in nodes_info}

        raw_data = stdout.split("\n")
        raw_data = [d.split(" SPLIT ") for d in raw_data if len(d) != 0]

        # Merge steps together
        jobs = []
        user_index = fields.index("User")
        for index, d in enumerate(raw_data):
            # Give back the control in order to deal with other requests
            if index % 100 == 0:
                await sleep(0)

            if len(d[user_index]) == 0:
                continue

            data = {k: d[i] for i, k in enumerate(fields)}

            index, job = extract_job_report(data, raw_data, fields, index, nodes_info)
            jobs.append(job)

        return jobs

    @staticmethod
    async def job_long_report(
        job_id: int,
        job_subtask: Union[int, None] = None,
    ) -> JobLongReportV2:
        # If any change happen here, please update the index in extract_job_report
        fields = [
            "User",
            "JobName",
            "NNodes",
            "NodeList",
            "AllocCPUs",
            "TotalCPU",
            "ElapsedRaw",
            "ReqMem",
            "MaxRSS",
            "AllocTRES",
            "JobID",
            "TRESUsageInTot",
            "State",
            "Reason",
            "Start",
            "Timelimit",
            "Account",
            "Constraints",
            "ConsumedEnergyRaw",
            "ExitCode",
            "QOS",
            "SubmitLine",
            "WorkDir",
            "Partition",
            "Submit",
            "NTask",
        ]

        job_id_field = job_id if job_subtask is None else f"{job_id}_{job_subtask}"
        cmd = [
            "sacct",
            "-P",
            "--format",
            ",".join(fields),
            f"-j{job_id_field}",
            "-n",
            "--delimiter= SPLIT ",
        ]

        stdout, _ = await submit_command(
            cmd, is_slurm=True, dev_output="tests/data/sacct_long_report.txt"
        )

        # Get info from nodes
        nodes_info = await Slurm.get_nodes()
        nodes_info = {n.name: n for n in nodes_info}

        raw_data = stdout.split("\n")
        raw_data = [d.split(" SPLIT ") for d in raw_data if len(d) != 0]

        # On dev, filter the right id
        if config.RUN_DEV:
            index = [
                i
                for i, d in enumerate(raw_data)
                if str(job_id_field) == d[fields.index("JobID")]
            ]
            if len(index) != 1:
                raise Exception("Found too many jobs for a single Job ID")
            index = index[0]
            new_raw_data = [raw_data[index]]
            while index + 1 < len(raw_data):
                index += 1
                if raw_data[index][fields.index("User")].strip() == "":
                    new_raw_data.append(raw_data[index])
                else:
                    break
            raw_data = new_raw_data

        # Merge steps together
        data = {k: raw_data[0][i] for i, k in enumerate(fields)}

        _, job = extract_job_report(data, raw_data, fields, 0, nodes_info)

        # Exit code
        exit_code, signal = data["ExitCode"].split(":")
        kwargs = {
            "account": data["Account"],
            "constraints": data["Constraints"],
            "consumed_energy_raw": 0,  # TODO
            "exit_code": int(exit_code),
            "signal": int(signal),
            "qos": data["QOS"],
            "work_dir": data["WorkDir"],
            "partition": data["Partition"],
            "submit_line": data["SubmitLine"],
        }
        kwargs.update(job.model_dump())
        job = JobLongReportV2(**kwargs)

        # Sacct does not provide correct data for pending and running jobs
        if job.state == JobState.RUNNING:
            ram, cpu = await Slurm.get_utilization_running_job(job.job_id)
            job.ram_util = ram * job.nodes / (1024**2 * job.memory_mb)
            job.cpu_util = cpu.total_seconds() / (job.cpus * job.elapsed_time_s)
        elif job.state == JobState.PENDING:
            job.start = await Slurm.get_start_pending_job(job.job_id)

        return job

    @staticmethod
    async def get_start_pending_job(job_id: int) -> datetime | None:
        cmd = [
            "squeue",
            "--Format=StartTime",
            f"-j{job_id}",
        ]

        stdout, _ = await submit_command(
            cmd, is_slurm=True, dev_output="tests/data/squeue_job_start.txt"
        )

        line = stdout.split("\n")[1].rstrip()
        return None if line == "N/A" else datetime.fromisoformat(line)

    @staticmethod
    async def get_utilization_running_job(job_id: int) -> tuple[int, timedelta]:
        fields = ["MaxRSS", "MinCPU"]
        cmd = [
            "sstat",
            "--all",
            "--format",
            ",".join(fields),
            f"-j{job_id}",
            "-n",
            "-P",
        ]

        stdout, _ = await submit_command(
            cmd, is_slurm=True, dev_output="tests/data/sstat_job.txt"
        )

        ram = 0
        cpu = timedelta()
        for line in stdout.split("\n"):
            if len(line) == 0:
                continue
            temp_ram, temp_cpu = line.split("|")
            ram = max(ram, get_memory_size(temp_ram))
            cpu = max(cpu, convert_time_to_timedelta(temp_cpu))
        return int(ram), cpu

    @staticmethod
    @alru_cache(ttl=60)
    async def get_partitions(partition_name: str | None = None) -> List[Partition]:
        """
        This function needs to be static for the cache to work
        """
        command = ["scontrol", "show", "partition"]
        if partition_name is not None:
            command.append(partition_name)

        stdout, _ = await submit_command(
            command, is_slurm=True, dev_output="tests/data/scontrol_partition.txt"
        )

        if partition_name is not None and "not found" in stdout:
            raise PartitionNotFound(partition_name)

        output = []
        for index, line in enumerate(stdout.split("\n\n")):
            # Give back the control in order to deal with other requests
            if index % 5 == 0:
                await sleep(0)

            if len(line) == 0:
                continue

            # Extract name
            name = extract_regex(r"(?<=PartitionName\=)[^ ]+", line)
            if name is None:
                raise Exception("Failed to find partition name")

            # With dev, we cannot request a single node, so process everything
            if config.RUN_DEV and partition_name is not None and name != partition_name:
                continue

            # Extract values
            nodes = extract_regex(r"(?<= Nodes\=)[^ ]+", line)
            nodes = extract_compact_node_names(nodes)
            total_cpus = extract_regex(r"(?<=TotalCPUs\=)[^ ]+", line)
            if total_cpus is None:
                raise Exception("Failed to find total cpus")
            total_nodes = extract_regex(r"(?<=TotalNodes\=)[^ ]+", line)
            if total_nodes is None:
                raise Exception("Failed to find total nodes")

            max_time = extract_regex(r"(?<=MaxTime\=)[^ ]+", line)
            if max_time is None:
                raise Exception("Failed to find MaxTime")
            if max_time == "UNLIMITED":
                max_time = None
            else:
                max_time = convert_time_to_timedelta(max_time)
            default_partition = extract_regex(r"(?<=Default\=)[^ ]+", line) == "YES"
            hidden = extract_regex(r"(?<=Hidden\=)[^ ]+", line) == "YES"

            state = extract_regex(r"(?<=State\=)[^ ]+", line)
            if state is None:
                raise Exception("Failed to find state")
            state = PartitionState.get_state_from_text(state)

            tres = extract_regex(r"(?<=TRES\=)[^ ]+", line)
            if tres is None:
                raise Exception("Failed to find tres")
            if tres != "(null)":
                tres = convert_slurm_list_to_dict(tres)
                res_name = "gres/gpu"
                total_gpus = 0 if res_name not in tres else int(tres[res_name])
            else:
                total_gpus = 0

            # Create the partition

            partition = Partition(
                name=name,
                state=state,
                nodes=nodes,
                total_cpus=total_cpus,
                total_nodes=total_nodes,
                total_gpus=total_gpus,
                max_time=max_time,
                default_partition=default_partition,
                hidden=hidden,
            )

            output.append(partition)

        return output

    @staticmethod
    @alru_cache(ttl=60)
    async def get_users() -> List[User]:
        """
        This function needs to be static for the cache to work
        """
        command = ["sacctmgr", "list", "users", "-P", "-n"]

        stdout, _ = await submit_command(
            command, is_slurm=True, dev_output="tests/data/sacctmgr_users.txt"
        )

        output = []
        for index, line in enumerate(stdout.split("\n")):
            # Give back the control in order to deal with other requests
            if index % 5 == 0:
                await sleep(0)

            if len(line) == 0:
                continue

            username = line.split("|")[0]
            user = User(
                username=username,
            )

            output.append(user)

        return output
