import asyncio
import re
from datetime import datetime, timedelta
from typing import Dict, List, Tuple, Union

from slurm_api_schema.job import JobReason, JobReportV2, JobState
from slurm_api_schema.node import NodeResponse

from .config import config
from .exceptions import FailedToProcessRequest
from .logger import logger
from .prometheus import Prometheus


async def submit_command(
    command: List[str],
    is_slurm: bool = False,
    is_health: bool = False,
    dev_output: Union[str, None] = None,
) -> Tuple[str, str]:
    if not is_health:
        logger.info(f"Running command: {' '.join(command)}")
    if config.RUN_DEV:
        stdout = ""
        stderr = ""
        if dev_output:
            with open(dev_output, "r") as f:
                stdout = "".join(f.readlines())
    else:
        proc = await asyncio.create_subprocess_exec(
            *command,
            limit=config.BUFFER_SIZE,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        code = await proc.wait()
        stdout, stderr = await proc.communicate()
        stdout = stdout.decode("utf8")
        stderr = stderr.decode("utf8")

        if code != 0:
            comm = " ".join(command)
            logger.error(f"Command: {comm}\nStdout: {stdout}\nStderr: {stderr}")
            raise FailedToProcessRequest(stdout, stderr)

    if not is_health:
        logger.debug(stdout.rstrip())
        if len(stderr) > 0:
            logger.warning(stderr.rstrip())

    # Metrics
    if is_slurm:
        Prometheus.increase_number_slurm_requests()
    if is_health:
        Prometheus.increase_number_slurm_health_requests()

    return stdout, stderr


def extract_regex(regex, text, default=None):
    m = re.search(regex, text)
    if m is None:
        return default
    return m.group(0).rstrip()


def convert_slurm_list_to_dict(text: str) -> dict:
    """
    Convert a slurm list (a=1,b=2,c=3) to a dict
    """
    if len(text) == 0:
        return {}
    out = text.split(",")
    out = [t.split("=") for t in out]
    out = {k: v for k, v in out}
    return out


def count_prepending_zeros(a: str) -> int:
    i = 0
    count = 0
    while i < len(a) and a[i] == "0":
        i += 1
        count += 1
    return count


def extract_compact_node_names(text) -> List[str]:
    """
    extract the list of nodes (e.g. node-01,node-0[3-4] to
    ['node-01', 'node-03', 'node-04'])
    """
    if text == "None assigned":
        return []
    nodes = []
    # First extract everything from within []
    while text.count("[") != 0:
        # Find the parenthesis to replace
        ind = text.find("[")
        ind2 = text.find("]", ind)

        # Find the common part of the name
        prev = text[:ind].split(",")[-1]

        # Get all 'extensions'
        subtext = text[ind + 1 : ind2]
        tmp = subtext.split(",")
        extensions = []
        for ext in tmp:
            # Full extension already provided
            if "-" not in ext:
                extensions.append(ext)
                continue

            # Need a bit more processing
            a, b = ext.split("-")
            b_prep = count_prepending_zeros(b) * "0"
            a = int(a)
            b = int(b)
            tmp = [b_prep + str(i) for i in range(a, b + 1)]
            tmp = ["0" + t if len(t) != len(tmp[-1]) else t for t in tmp]
            extensions.extend(tmp)

        full_names = [ext if i == 0 else prev + ext for i, ext in enumerate(extensions)]
        text = text[:ind] + ",".join(full_names) + text[ind2 + 1 :]

    nodes = text.split(",")
    return nodes


def convert_time_to_timedelta(text: str) -> timedelta:
    """
    Convert a slurm time to seconds.
    For example the following strings are correctly handled:
    - 2-19:24:00
    - 19:24:00
    - 24:00
    """
    templates = [
        "%j-%H:%M:%S",
        "%H:%M:%S",
        "%M:%S",
        "%M:%S.%f",
    ]
    for template in templates:
        try:
            time = datetime.strptime(text, template)
        except ValueError:
            continue
        if "%j" in template:
            ref = datetime(year=1899, month=12, day=31, hour=0, minute=0)
        else:
            ref = datetime(year=1900, month=1, day=1, hour=0, minute=0)
        delta = time - ref
        return delta

    # Try manually extracting the info
    regex = re.compile(r"\d+-\d+:\d+:\d+")
    if regex.match(text):
        days, hours, minutes, seconds = re.findall(r"\d+", text)
        return timedelta(
            days=int(days), hours=int(hours), minutes=int(minutes), seconds=int(seconds)
        )

    raise Exception(f"Failed to convert time '{text}'")


def get_memory_size(text: str):
    """
    Convert slurm memory string to integer.
    """
    if len(text) == 0 or text == "0":
        return 0
    else:
        unit = text[-1]
        if unit.isdigit():
            return float(text)

        value = float(text[:-1])
        if unit == "P":
            value *= 1024**5
        elif unit == "T":
            value *= 1024**4
        elif unit == "G":
            value *= 1024**3
        elif unit == "M":
            value *= 1024**2
        elif unit == "K":
            value *= 1024
        else:
            raise Exception(f"Unknown unit: {unit}")
        return value


def extract_job_report(
    d: dict[str, str],
    raw_data: list[list[str]],
    fields: list,
    index: int,
    nodes_info: Dict[str, NodeResponse],
) -> Tuple[int, JobReportV2]:
    # Slurm Keys
    GPU_AMOUNT = "gres/gpu"
    GPU_UTIL = "gres/gpuutil"
    GPU_RAM = "gres/gpumem"

    # Convert data
    alloc_tres = convert_slurm_list_to_dict(d["AllocTRES"])
    tres_usage = convert_slurm_list_to_dict(d["TRESUsageInTot"])

    # CPU
    nodes = int(d["NNodes"])

    cpus = int(d["AllocCPUs"])
    total_cpu = convert_time_to_timedelta(d["TotalCPU"]).total_seconds()
    elapsed_raw = int(d["ElapsedRaw"])
    cpu_util = 1 if elapsed_raw == 0 else total_cpu / (cpus * elapsed_raw)

    ram = get_memory_size(d["ReqMem"])
    ram_mb = ram / (1024**2)
    ntask = int(d["NTask"] if len(d["NTask"]) > 0 else 1)
    max_ram_used = get_memory_size(d["MaxRSS"]) * ntask

    # GPU
    gpus = 0 if GPU_AMOUNT not in alloc_tres else int(alloc_tres[GPU_AMOUNT])
    gpu_util = 0.0
    gpu_ram_util = (
        0 if GPU_RAM not in tres_usage else get_memory_size(tres_usage[GPU_RAM])
    )

    # Get info from nodes (assume all nodes are the same)
    nodes_list = extract_compact_node_names(d["NodeList"])
    valid_nodes = len(nodes_list) != 0 and nodes_list[0] in nodes_info
    if valid_nodes:
        ref_node = nodes_info[nodes_list[0]]
        cpu_fraction_node = cpus / (nodes * ref_node.number_cores)
        ram_fraction_node = ram_mb / (nodes * ref_node.memory_mb)
        gpu_fraction_node = 0 if gpus == 0 else gpus / (nodes * ref_node.number_gpus)
        gpu_memory_mb = ref_node.gpu_memory_per_gpu_mb * gpus
    else:
        cpu_fraction_node = -1
        ram_fraction_node = -1
        gpu_fraction_node = -1
        gpu_memory_mb = 0

    # Account all the steps
    i = index + 1
    USER = fields.index("User")
    MAX_VM_SIZE = fields.index("MaxRSS")
    NTASK = fields.index("NTask")
    TRES_USAGE_IN_TOT = fields.index("TRESUsageInTot")
    ELAPSED_RAW = fields.index("ElapsedRaw")

    while i < len(raw_data) and len(raw_data[i][USER].strip()) == 0:
        ntask = raw_data[i][NTASK]
        ntask = int(ntask if len(ntask) > 0 else 1)
        max_ram_used += get_memory_size(raw_data[i][MAX_VM_SIZE]) * ntask

        tres = convert_slurm_list_to_dict(raw_data[i][TRES_USAGE_IN_TOT])
        if GPU_UTIL in tres:
            gpu_util += float(tres[GPU_UTIL]) * float(raw_data[i][ELAPSED_RAW])

        if GPU_RAM in tres:
            gpu_ram_util = max(gpu_ram_util, get_memory_size(tres[GPU_RAM]))

        i += 1

    # Create variables depending on previous results
    ram_util = max_ram_used / ram

    # Normalized util
    gpu_util = 1 if elapsed_raw == 0 else gpu_util / (100 * elapsed_raw)
    if valid_nodes:
        gpu_ram_util = (
            1
            if ref_node.gpu_memory_per_gpu_mb == 0
            else gpu_ram_util / (1024**2 * ref_node.gpu_memory_per_gpu_mb)
        )
    else:
        gpu_ram_util = 1

    # Time
    time_limit = convert_time_to_timedelta(d["Timelimit"])
    try:
        time_start = datetime.fromisoformat(d["Start"])
    except ValueError:
        time_start = None
    # TODO compute it based on local time
    time_left = None
    submit_time = datetime.fromisoformat(d["Submit"].strip())

    # State
    state = JobState.get_state_from_text(d["State"])
    reason = JobReason.get_reason_from_text(d["Reason"])
    reason_raw = d["Reason"]

    # Job ID
    if "_" in d["JobID"]:
        job_id, job_tasks = d["JobID"].split("_")
        job_id = int(job_id)
    else:
        job_id = int(d["JobID"])
        job_tasks = ""

    # Energy consumed
    try:
        consumed_energy_raw = float(d["ConsumedEnergyRaw"])
    except ValueError:
        consumed_energy_raw = 0

    return index, JobReportV2(
        user=d["User"].strip(),
        name=d["JobName"].strip(),
        job_id=job_id,
        job_tasks=job_tasks,
        state=state,
        reason=reason,
        reason_raw=reason_raw,
        threads=-1,
        cpus=cpus,
        nodes=nodes,
        memory_mb=int(ram_mb),
        start=time_start,
        time_left=time_left,
        time_limit=time_limit,
        cpu_util=cpu_util,
        ram_util=ram_util,
        elapsed_time_s=elapsed_raw,
        nodes_list=nodes_list,
        cpu_fraction_node=cpu_fraction_node,
        ram_fraction_node=ram_fraction_node,
        gpus_fraction_node=gpu_fraction_node,
        gpus=gpus,
        gpu_memory_mb=gpu_memory_mb,
        gpu_ram_util=gpu_ram_util,
        gpu_util=gpu_util,
        consumed_energy_raw=consumed_energy_raw,
        submit_time=submit_time,
    )
