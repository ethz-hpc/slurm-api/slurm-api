from slurm_api.endpoint import Endpoint


class TestEndpoint:
    def test_endpoint(self):
        e1 = Endpoint("GET", "/path")
        e2 = Endpoint("PUT", "/path")
        assert e1 != e2
        assert e2 != e1

        e3 = Endpoint("GET", "/path")
        assert e3 == e1
        assert e1 == e3

        e4 = Endpoint("GET", "/path/*/to")
        e5 = Endpoint("GET", "/path/asbc_cd/to")
        assert e4 == e5
        assert e5 != e4

        e6 = Endpoint("GET", "/path/asd/sada/to")
        assert e4 != e6
        assert e6 != e4

        endpoints = [e2, e3, e4, e5, e6]
        assert e1 in endpoints
