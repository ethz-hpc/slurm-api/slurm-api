from unittest.mock import patch

from fastapi import FastAPI, status
from fastapi.testclient import TestClient

from slurm_api.authentication import authentication
from slurm_api.config import config
from slurm_api.endpoint import Endpoint

auth_header_name = "test"
auth_rules = {
    "authorized": {"key": "1234", "endpoints": [Endpoint("GET", "/nodes")]},
    "authorized-star": {"key": "12", "endpoints": [Endpoint("GET", "/nodes/*")]},
    "not-authorized": {"key": "123456", "endpoints": []},
}


class TestAuthentication:
    @patch.object(config, "AUTH_RULES", auth_rules, create=True)
    @patch.object(config, "AUTH_HEADER_NAME", auth_header_name, create=True)
    @patch.object(config, "RUN_DEV", False)
    def test_authentication(self):
        app = FastAPI()
        app.middleware("http")(authentication)

        @app.get("/nodes")
        def default():
            return "ok"

        @app.get("/nodes/{node_name}")
        def default2(node_name: str):
            return "ok"

        client = TestClient(app)

        # Test unauthorized access without api key
        response = client.get("/nodes")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Test wrong api key
        response = client.get(
            "/nodes",
            headers={auth_header_name: auth_rules["authorized"]["key"] + "x"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Test a protected endpoint
        response = client.get(
            "/nodes",
            headers={auth_header_name: auth_rules["authorized"]["key"]},
        )
        assert response.status_code == status.HTTP_200_OK

        # Test jupyter cannot reach monkey endpoints
        response = client.get(
            "/nodes",
            headers={auth_header_name: auth_rules["not-authorized"]["key"]},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Test non star cannot reach star
        response = client.get(
            "/nodes/abc",
            headers={auth_header_name: auth_rules["authorized"]["key"]},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Test star cannot reach non star
        response = client.get(
            "/nodes",
            headers={auth_header_name: auth_rules["authorized-star"]["key"]},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Test star can reach star
        response = client.get(
            "/nodes/abc",
            headers={auth_header_name: auth_rules["authorized-star"]["key"]},
        )
        assert response.status_code == status.HTTP_200_OK
