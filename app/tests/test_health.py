import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient

from slurm_api.routers.health import router


@pytest.mark.asyncio
async def test_health():
    app = FastAPI()
    app.include_router(router)
    client = TestClient(app)

    response = client.get("/health")
    assert response.status_code == 200
    assert response.json() is True

    response = client.get("/health-k8s")
    assert response.status_code == 200
    assert response.json() is True
