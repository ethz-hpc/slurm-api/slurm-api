from slurm_api.utils import (
    convert_time_to_timedelta,
    extract_compact_node_names,
    get_memory_size,
)


class TestUtils:
    def test_extract_nodes(self):
        tests = {
            "node-1-049": ["node-1-049"],
            "node-1-04[8-9]": ["node-1-048", "node-1-049"],
            "node-1-048,node-1-049": ["node-1-048", "node-1-049"],
            "node-1-04[8-9],node-1-04[3-4]": [
                "node-1-048",
                "node-1-049",
                "node-1-043",
                "node-1-044",
            ],
            "node-1-0[50,52,53],node-1-04[3-4]": [
                "node-1-050",
                "node-1-052",
                "node-1-053",
                "node-1-043",
                "node-1-044",
            ],
            "node-1-0[50,52-55],node-1-04[3-4]": [
                "node-1-050",
                "node-1-052",
                "node-1-053",
                "node-1-054",
                "node-1-055",
                "node-1-043",
                "node-1-044",
            ],
            "node-[072,099-100]": ["node-072", "node-099", "node-100"],
        }
        for key in tests.keys():
            result = extract_compact_node_names(key)
            assert len(result) == len(tests[key])
            for i, element in enumerate(result):
                assert element == tests[key][i]

    def test_convert_time_to_s(self):
        tests = {
            "2-19:24:00": 242640,
            "19:24:00": 69840,
            "24:00": 1440,
            "52-19:20:00": 4562400,
            "52-19:20:00": 4562400,
            "9503-07:49:04": 821087344,
        }
        for text in tests.keys():
            result = convert_time_to_timedelta(text).total_seconds()
            assert result == tests[text]

    def test_get_memory_size(self):
        test = {
            "3.2T": 1024**4 * 3.2,
            "1020M": 1024**2 * 1020,
            "12K": 1024 * 12,
            "3.4G": 1024**3 * 3.4,
            "5": 5,
        }
        for key, ref in test.items():
            value = get_memory_size(key)
            assert ref == value
