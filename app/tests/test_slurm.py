from datetime import datetime, timedelta

import pytest
from fastapi import HTTPException

from slurm_api.slurm import Slurm
from slurm_api_schema.job import JobReason, JobState
from slurm_api_schema.node import Node, NodeState
from slurm_api_schema.partition import PartitionState
from slurm_api_schema.reboot import Reboot, RebootNextState, RebootType
from slurm_api_schema.reservation import Reservation, ReservationFlag
from slurm_api_schema.submit import Submit


class TestSlurm:
    @pytest.mark.asyncio
    async def test_get_nodes(self):
        slurm = Slurm()
        output = await slurm.get_nodes()

        # Get to a node with a reason
        node = output[15]
        assert node.name == "ctrl-4t-01"
        assert node.group == "AMD,EPYC_7742,ethernet,(null),cpus=128,ram=4128000"
        assert node.state == NodeState.DOWN | NodeState.DRAIN | NodeState.NOT_RESPONDING
        assert node.boot_time == datetime(
            year=2023, month=1, day=2, hour=17, minute=45, second=12
        )
        assert node.reason == "REASON5"

        types = set()
        for node in output:
            types.add(node.group)

    @pytest.mark.asyncio
    async def test_set_nodes(self):
        slurm = Slurm()
        node = Node(name="test", reason="1234", state=NodeState.DRAIN)
        # Cannot really test anything here
        await slurm.set_node(node)

    @pytest.mark.asyncio
    async def test_get_reservations(self):
        slurm = Slurm()
        output = await slurm.get_reservations()
        assert len(output) == 6

        assert output[-1].name == "root_48"
        assert len(output[-1].nodes) == 1
        assert output[-1].nodes == ["node"]
        assert output[-1].start == datetime.fromisoformat("2024-02-06T05:00:00")
        assert output[-1].duration_min == 120
        assert len(output[-1].users) == 1
        assert output[-1].users == ["root"]
        assert (
            output[-1].flags
            == ReservationFlag.MAINT
            | ReservationFlag.IGNORE_JOBS
            | ReservationFlag.SPEC_NODES
        )

    @pytest.mark.asyncio
    async def test_set_reservation(self):
        slurm = Slurm()
        res = Reservation(
            name="",
            nodes=["node"],
            start=None,
            duration_min=120,
            users=["root"],
            flags=ReservationFlag.MAINT,
        )
        output = await slurm.set_reservation(res)
        assert output == "root_1"

        res.name = "root_2"
        output = await slurm.set_reservation(res)
        assert output == "root_1"

    @pytest.mark.asyncio
    async def test_reboot(self):
        slurm = Slurm()
        # Nothing to really check, ensure just that it runs
        reboot = Reboot(
            nodes=["tester"],
            reboot_type=RebootType.ASAP,
            next_state=RebootNextState.RESUME,
        )
        await slurm.reboot(reboot)

    @pytest.mark.asyncio
    async def test_queue_statistics(self):
        slurm = Slurm()
        output = await slurm.get_queue_statistics()

        assert output.users["viso"] == 9
        assert output.reasons[JobReason.Resources] == 2
        assert output.reasons[JobReason.QOSMax] == 41

    @pytest.mark.asyncio
    async def test_cancel_job(self):
        slurm = Slurm()
        # Nothing to really check, ensure just that it runs
        await slurm.cancel_job(1)

    @pytest.mark.asyncio
    async def test_submit_job(self):
        slurm = Slurm()
        # Nothing to really check, ensure just that it runs
        submit = Submit(user="calculus", script="#!/bin/bash\necho Test")
        await slurm.submit_job(submit)

        with pytest.raises(HTTPException):
            submit.user = "root"
            await slurm.submit_job(submit)

    @pytest.mark.asyncio
    async def test_job_status(self):
        slurm = Slurm()
        # Pending
        # run_main_local_scratch_slurm.sh PENDING (QOSMaxMemoryPerUser) * 120 1
        # 12.5G N/A 3-08:00:00 3-08:00:00
        job_id = 9562347
        job = await slurm.job_status(job_id)
        assert job.name == "run_main_local_scratch_slurm.sh"
        assert job.job_id == job_id
        assert job.state == JobState.PENDING
        assert job.reason == JobReason.QOSMax
        assert job.threads == -1
        assert job.cpus == 120
        assert job.nodes == 1
        assert job.memory_mb == 12500
        assert job.start is None
        assert job.time_left == timedelta(days=3, hours=8)
        assert job.time_limit == timedelta(days=3, hours=8)

        # Running
        # subhalos_job_fullsky_2048_SGC RUNNING node-009-1 * 128
        # 1 3900M 2023-02-13T09:55:39 10:55:31 12:00:00 1234
        job_id = 1234
        job = await slurm.job_status(job_id)
        assert job.name == "subhalos_job_fullsky_2048_SGC"
        assert job.job_id == job_id
        assert job.state == JobState.RUNNING
        assert job.reason == JobReason.NONE
        assert job.threads == -1
        assert job.cpus == 128
        assert job.nodes == 1
        assert job.memory_mb == 3900
        assert job.start == datetime(
            year=2023, month=2, day=13, hour=9, minute=55, second=39
        )
        assert job.time_left == timedelta(hours=10, minutes=55, seconds=31)
        assert job.time_limit == timedelta(hours=12)

        # Invalid time left
        job_id = 18450608
        job = await slurm.job_status(job_id)
        assert job.time_left is None

        # Multiple words in reason
        job_id = 18627966
        job = await slurm.job_status(job_id)
        assert (
            job.reason_raw == "(Nodes required for job are DOWN, DRAINED or reserved"
            " for jobs in higher priority partitions)"
        )

    @pytest.mark.asyncio
    async def test_get_jobs_report(self):
        slurm = Slurm()

        # A bit hard to test anything
        # => ensure that it does not fail
        jobs = await slurm.get_jobs_report(
            for_last_hours=24,
            user=None,
            state=None,
        )

        # Req 250G, 2 nodes
        # 0
        # 89647032K * 2
        # 0
        # 93936748K * 1
        ram_util = 89647032 * 2 + 93936748
        ram_util /= 1024 * 1024 * 250
        assert abs(jobs[1].ram_util - ram_util) < 1e-6

    @pytest.mark.asyncio
    async def test_get_job_long_report(self):
        slurm = Slurm()

        # Test normal job
        job = await slurm.job_long_report(34022398)
        assert job.user == "acc"
        assert job.qos == "qos1"

        # Test job array
        job = await slurm.job_long_report(34020680, 40)
        assert job.user == "abb"
        assert job.qos == "normal/account1"

        # Test mpi job
        job = await slurm.job_long_report(51927852)
        assert job.user == "dsa"
        assert abs(job.ram_util - 0.351525926) < 1e-5

    @pytest.mark.asyncio
    async def test_get_partition(self):
        slurm = Slurm()
        output = await slurm.get_partitions()

        # Check first partition
        p = output[0]
        assert p.name == "p1.4h"
        assert p.state == PartitionState.UP
        assert p.nodes == ["node-01", "node-02", "node-001", "node-002", "node-003"]
        assert p.total_cpus == 504
        assert p.total_nodes == 10
        assert p.total_gpus == 6
        assert p.max_time == timedelta(days=5)
        assert not p.default_partition
        assert p.hidden

        # Check second
        p = output[1]
        assert p.name == "p2"
        assert not p.hidden
        assert p.total_gpus == 0
        assert p.state == PartitionState.DOWN
        assert p.max_time is None

        # Check third
        p = output[2]
        assert p.name == "p3"
        assert p.default_partition
        assert p.state == PartitionState.DRAIN

        output = await slurm.get_partitions("p3")
        assert output[0].name == "p3"

    @pytest.mark.asyncio
    async def test_get_start_pending_job(self):
        slurm = Slurm()
        output = await slurm.get_start_pending_job(1)
        assert output == datetime(
            year=2023, month=12, day=5, hour=20, minute=5, second=15
        )

    @pytest.mark.asyncio
    async def test_get_utilization_running_job(self):
        slurm = Slurm()
        ram, cpu = await slurm.get_utilization_running_job(1)
        assert cpu == timedelta(minutes=1)
        assert ram == 328 * 1024

    @pytest.mark.asyncio
    async def test_get_users(self):
        slurm = Slurm()
        users = await slurm.get_users()
        for i, l in enumerate(["a", "b", "c", "d", "e", "f"]):
            assert users[i].username == l * 2
