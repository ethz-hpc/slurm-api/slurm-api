from datetime import datetime, timedelta

import pytest
from pydantic import ValidationError

from slurm_api_schema.job import JobReason, JobState, JobV2
from slurm_api_schema.node import Node, NodeState
from slurm_api_schema.reboot import Reboot, RebootType
from slurm_api_schema.reservation import Reservation, ReservationFlag
from slurm_api_schema.statistics import QueueStatistics
from slurm_api_schema.submit import Submit


class TestSlurm:
    def test_nodes_state(self):
        assert NodeState.CLOUD.is_open()
        assert (
            NodeState.IDLE | NodeState.MIXED | NodeState.PLANNED | NodeState.RESERVED
        ).is_open()
        assert not NodeState.DRAINED.is_open()
        assert not (NodeState.DRAINED | NodeState.FAIL).is_open()
        assert not NodeState.NONE.is_open()

    def test_get_state_from_text(self):
        assert NodeState.get_state_from_text("MIX") == NodeState.MIX
        assert NodeState.get_state_from_text("IDLE") == NodeState.IDLE
        assert (
            NodeState.get_state_from_text("IDLE+FAIL")
            == NodeState.IDLE | NodeState.FAIL
        )

    def test_reservation_convert(self):
        data = {
            "MAINT,HOURLY": ReservationFlag.MAINT | ReservationFlag.HOURLY,
            "MAINT,HOURLY,PART_NODES": ReservationFlag.MAINT
            | ReservationFlag.HOURLY
            | ReservationFlag.PART_NODES,
            "": ReservationFlag.NONE,
            "OVERLAP": ReservationFlag.OVERLAP,
        }

        for text, flag in data.items():
            assert ReservationFlag.get_text(flag) == text
            assert ReservationFlag.get_flags(text) == flag

    def test_job_reason(self):
        data = {
            "(Priority)": JobReason.Priority,
            "(QOSMaxCpuPerUserLimit)": JobReason.QOSMax,
            "(Dependency)": JobReason.Dependency,
        }
        for key, value in data.items():
            assert JobReason.get_reason_from_text(key) == value

    def test_string_sanitizer(self):
        string_ok = "root"
        string_not_ok = r"\dsae"

        job_kwargs = {
            "job_tasks": "",
            "state": JobState.BOOT_FAIL,
            "reason": JobReason.Dependency,
            "reason_raw": "test",
            "threads": 1,
            "cpus": 1,
            "nodes": 1,
            "memory_mb": 1,
            "start": None,
            "time_left": timedelta(hours=1),
            "time_limit": timedelta(hours=1),
            "submit_time": datetime.now(),
        }
        JobV2(name=string_ok, job_id=1, **job_kwargs)
        with pytest.raises(ValidationError):
            JobV2(name=string_not_ok, **job_kwargs)

        node_kwargs = {
            "state": NodeState.ALLOC,
        }
        Node(name=string_ok, group=string_ok, reason=string_ok, **node_kwargs)
        with pytest.raises(ValidationError):
            Node(name=string_not_ok, group=string_ok, reason=string_ok, **node_kwargs)
        with pytest.raises(ValidationError):
            Node(name=string_ok, group=string_not_ok, reason=string_ok, **node_kwargs)
        with pytest.raises(ValidationError):
            Node(name=string_ok, group=string_ok, reason=string_not_ok, **node_kwargs)

        queue_kwargs = {"suspicious_priority": 1, "reasons": {JobReason.AssocMax: 1}}
        QueueStatistics(users={string_ok: 1}, **queue_kwargs)
        with pytest.raises(ValidationError):
            QueueStatistics(users={string_not_ok: 1}, **queue_kwargs)

        reboot_kwargs = {
            "reboot_type": RebootType.NORMAL,
        }
        Reboot(nodes=[string_ok], reason=string_ok, **reboot_kwargs)
        with pytest.raises(ValidationError):
            Reboot(nodes=[string_not_ok], reason=string_ok, **reboot_kwargs)
        with pytest.raises(ValidationError):
            Reboot(nodes=[string_ok], reason=string_not_ok, **reboot_kwargs)

        reservation_kwargs = {
            "duration_min": 1,
            "flags": ReservationFlag.NONE,
        }
        Reservation(
            name=string_ok, nodes=[string_ok], users=[string_ok], **reservation_kwargs
        )
        with pytest.raises(ValidationError):
            Reservation(
                name=string_not_ok,
                nodes=[string_ok],
                users=[string_ok],
                **reservation_kwargs
            )
        with pytest.raises(ValidationError):
            Reservation(
                name=string_ok,
                nodes=[string_not_ok],
                users=[string_ok],
                **reservation_kwargs
            )
        with pytest.raises(ValidationError):
            Reservation(
                name=string_ok,
                nodes=[string_ok],
                users=[string_not_ok],
                **reservation_kwargs
            )

        submit_kwargs = {"script": ""}
        Submit(user=string_ok, **submit_kwargs)
        with pytest.raises(ValidationError):
            Submit(user=string_not_ok, **submit_kwargs)
