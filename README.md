# slurm-api
[![pipeline status](https://gitlab.com/ethz-hpc/slurm-api/slurm-api/badges/main/pipeline.svg)](https://gitlab.com/ethz-hpc/slurm-api/slurm-api/-/commits/main)[![coverage report](https://gitlab.com/ethz-hpc/slurm-api/slurm-api/badges/main/coverage.svg)](https://gitlab.com/ethz-hpc/slurm-api/slurm-api/-/commits/main)

Merge requests to this repository are appreciated.

This repository contains a code to provide a REST API to slurm. It implements some basic authentication / authorization based on API keys.

This repository uses a Makefile in order to describe all the useful commands. To start using it, use `make app-install-poetry  app-install-libraries app-init app-run-prod`.

## Deployment

We provide a container in this repository registry and also an helm chart (kubernetes) in `chart`. If you have any other deployment type, feel free to open a merge request.
LDAP and Slurm are very specific to all clusters, so the base container does not contain any configuration. It does not even install slurm as cluster might have different version of slurm.

## Testing

In order to quickly test some new features, some outputs to slurm commands are available in `app/tests/data`. You can run the server and provide the results from those outputs by running: `make app-run`.

## Adding a new python library

  cd app
  poetry add LIBRARY
  poetry add LIBRARY [--group dev]

## Removing a python library

  cd app
  poetry remove LIBRARY
