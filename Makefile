.ONESHELL:
HADOLINT_VERSION ?= 2.12.0
TRIVY_VERSION ?= 0.37.1

DOCKER ?= docker
REGISTRY ?= registry.gitlab.com/ethz-hpc/slurm-api
PORT ?= 4225
CONFIG_PATH ?= ../config.yaml
TIMEOUT ?= 30
ARGS ?= ""
ADM_USER = adm-nkowenski
all:

install-kubelinter:
	GO111MODULE=on go install golang.stackrox.io/kube-linter/cmd/kube-linter@latest

install-poetry:
	pip3 install poetry

install-container-tools:
	set -e
	wget https://github.com/hadolint/hadolint/releases/download/v$(HADOLINT_VERSION)/hadolint-Linux-x86_64
	mv hadolint* hadolint
	wget https://github.com/aquasecurity/trivy/releases/download/v$(TRIVY_VERSION)/trivy_$(TRIVY_VERSION)_Linux-64bit.tar.gz
	tar -xvf trivy*tar.gz
	chmod a+x hadolint trivy

app-update-dependencies:
	cd app
	poetry update

app-install-libraries:
	cd app
	poetry install

app-install-prod-libraries:
	cd app
	poetry install --without=dev --no-cache

app-init: install-poetry app-update-dependencies app-install-libraries
	git submodule update --init --recursive

app-run:
	cd app
	export CONFIG_PATH=$(CONFIG_PATH)
	RUN_DEV=1 poetry run uvicorn slurm_api.main:app --reload --port ${PORT}  #  --log-level trace

app-run-prod:
	cd app
	export CONFIG_PATH=$(CONFIG_PATH)
	poetry run unicorn --host 0.0.0.0 --port ${PORT} --timeout-keep-alive $(TIMEOUT) slurm_api.main:app

app-test:
	set -e
	cd app
	export RUN_DEV=1
	export CONFIG_PATH=$(CONFIG_PATH)
	poetry run coverage run -m pytest $(ARGS)
	poetry run coverage report
	poetry run coverage xml
	poetry run bandit -r .
	poetry run safety check

app-test-requests:
	./requests.sh

app-check-format:
	set -e
	cd app
	poetry run black --check .
	poetry run isort --check-only --profile black .
	poetry run flake8 .

app-format:
	cd app
	poetry run black .
	poetry run isort --profile black .
	poetry run flake8  .

container-build:
	$(DOCKER) build --pull \
		--build-arg http_proxy="${http_proxy}" --build-arg https_proxy="${https_proxy}" \
		--build-arg no_proxy="${no_proxy}" \
		-f container/Dockerfile . -t slurm-api:latest $(EXTRA_ARGS)

container-hadolint-check:
	hadolint container/Dockerfile
	echo Success

container-trivy-check:
# You can avoid installing it by creating a script with the following line: docker run  -v /var/run/docker.sock:/var/run/docker.sock -v $$HOME/.trivy:/root/.cache/ docker.io/aquasec/trivy $@
	trivy image slurm-api:latest --exit-code 1 ${EXTRA_ARGS}

container-test: container-hadolint-check container-trivy-check

container-push:
	$(DOCKER) tag slurm-api:latest $(REGISTRY)/slurm-api:latest
	$(DOCKER) push $(REGISTRY)/slurm-api:latest

container-pull:
	$(DOCKER) pull $(REGISTRY)/slurm-api:latest

chart-compile:
	cd chart
	helm template . --namespace slurm-api --values values.yaml --debug > ../all.yaml

chart-check: chart-compile
	yamllint -s all.yaml
	~/go/bin/kube-linter lint all.yaml

registry-logout:
	${DOCKER} logout $(REGISTRY)

registry-login:
	${DOCKER} login $(REGISTRY)

registry-login-pipeline:
	${DOCKER} login $(REGISTRY) --username '${REGISTRY_ACCOUNT}' --password '${REGISTRY_PASSWORD}'

update-test-data:
	ssh $(ADM_USER)@eu-login-10.euler.ethz.ch "scontrol show reservations > /cluster/home/${ADM_USER}/res-latest"
	ssh ${ADM_USER}@eu-login-10.euler.ethz.ch "scontrol show nodes > /cluster/home/${ADM_USER}/nodes-latest"
	scp ${ADM_USER}@eu-login-10.euler.ethz.ch:/cluster/home/${ADM_USER}/res-latest ~/gitLab/ethz-hpc/slurm-api/app/tests/data/scontrol_reservation.txt
	scp ${ADM_USER}@eu-login-10.euler.ethz.ch:/cluster/home/${ADM_USER}/nodes-latest ~/gitLab/ethz-hpc/slurm-api/app/tests/data/scontrol_node.txt
